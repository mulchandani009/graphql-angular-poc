import {Component, OnInit} from '@angular/core';
import {Apollo} from 'apollo-angular';
import gql from 'graphql-tag';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'graphql-angular-poc';
  allLinks: any;
  loading = true;
  error: any;

  constructor(private apollo: Apollo) {}

  ngOnInit() {
    this.apollo
      .watchQuery({
        query: gql`
				         {
				  allLinks {
				    id
				    description
				    url
				  }
				}
        `,
      })
      .valueChanges.subscribe(result => {
      	console.log(result);
        this.allLinks = result.data;
      	if (this.allLinks)
      		this.allLinks = this.allLinks.allLinks;
        this.loading = result.loading;
      },
      error => {
        this.error = error;
      },
      () => {
        // 'onCompleted' callback.
        // No errors, route to new page here
      });
  }
}
